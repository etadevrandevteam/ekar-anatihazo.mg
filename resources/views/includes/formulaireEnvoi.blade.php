<div class="col-8">
  @if (isset($destinataire))

<form class="" action="{{ route("send") }}" method="post">
          <div class="form-group row">
            <label for="staticName" class="col-sm-2 col-form-label">Nom</label>
            <div class="col-sm-10">
              <input type="text" name="name" readonly class="form-control-plaintext" id="staticName" value="{{ $destinataire->name ?? 'nom' }}">
            </div>
            <label for="staticEmail" class="col-sm-2 col-form-label">Adresse mail</label>
            <div class="col-sm-10">
              <input type="text" name="mail" readonly class="form-control-plaintext" id="staticEmail" value="{{ $destinataire->mail ?? 'email' }}">
            </div>
          </div>
          <div class="form-group row">
            <label for="inputContent" class="col-sm-2 col-form-label">Contenu</label>
            <div class="col-sm-10">
              <textarea name="inputContent" id="inputContent" class="form-control" rows="8" cols="80"></textarea>
            </div>
          </div>
          <input class="btn btn-primary" type="submit" name="" value="Envoyer">
          @csrf
      </form>

  @else

    <h5>veuillez choisir un destinataire a gauche</h5>

  @endif
</div>

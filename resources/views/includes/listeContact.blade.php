<div class="col-4">
  <h1>Liste Contacts</h1>
  <div class="list-group list-group-flush">
    @foreach($contacts as $contact)
      <a href="/sendTo/{{ $contact->slug }}" class="list-group-item list-group-item-action bg-light">{{ $contact->name }}</a>
    @endforeach
  </div>

</div>

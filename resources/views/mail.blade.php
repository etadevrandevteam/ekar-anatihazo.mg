<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="../../public/css/bootstrap.min.css" rel="stylesheet">
    <title></title>
  </head>
  <body>
      @if(isset($message))
      <h1>{{ $message }}</h1>
      @endif

      <div class="d-flex" id="wrapper">
        <div class="container row">
          @include('includes.listeContact')

          @include('includes.formulaireEnvoi')
        </div>
      </div>

  </body>
</html>

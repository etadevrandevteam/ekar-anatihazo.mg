<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



/**
 * Controller for sending mail to user
 */
class MailingController extends Controller
{
    //
    public function index(){
      $contacts = Contact::all();

      return view('mail', ['contacts' => $contacts]);
    }
    public function sendTo($slug){
      $contacts = Contact::all();
      $destinataire = Contact::where('slug', '=', $slug)->first();

      //return view('mail')->with('destinataire', $destinataire);
      return view('mail')->with(['destinataire' => $destinataire, 'contacts' => $contacts]);
    }
    public function sendMail(Request $request){
        $contact = new Contact();
        $contact->name = $request['name'];
        $contact->mail = $request['mail'];
        $contact->slug = "user-" . $request['name'];
        if($contact->save()){
            return redirect()->route('mail')->with('message', 'success');
        }
    }
}
